﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadifySvcClient
{
    class Program
    {
        static void Main(string[] args)
        {
            // Reference implementation

            using (Readify.RedPillClient client = new Readify.RedPillClient())
            {
                var bigFib = client.FibonacciNumber(30);
                var megaFib = client.FibonacciNumber(92);
                var negativeFib = client.FibonacciNumber(-3);

                var tokenResult = client.WhatIsYourToken();

                var words1 = client.ReverseWords("hello there!");
                var words2 = client.ReverseWords(string.Empty);
                var words3 = client.ReverseWords("James Charters ");

                var triangle1 = client.WhatShapeIsThis(2, 2, 3);
                var triangle2 = client.WhatShapeIsThis(12, 10, 12);
                var triangle3 = client.WhatShapeIsThis(2, 3, 4);

                //Console.WriteLine(bigFib);
                Console.WriteLine(tokenResult);
                Console.WriteLine(words1);
                Console.WriteLine(words2);
                Console.WriteLine(triangle1);
                Console.WriteLine(triangle2);
                Console.WriteLine(words3);
            }

            // Custom
            using (CustomRedPillSvc.RedPillClient client = new CustomRedPillSvc.RedPillClient())
            {
                var bigFib = client.FibonacciNumber(30);
                var megaFib = client.FibonacciNumber(92);
                var negativeFib = client.FibonacciNumber(-3);

                var tokenResult = client.WhatIsYourToken();

                var words1 = client.ReverseWords("hello there!");
                var words2 = client.ReverseWords(string.Empty);
                var words3 = client.ReverseWords("James Charters ");

                var triangle1 = client.WhatShapeIsThis(2, 2, 3);
                var triangle2 = client.WhatShapeIsThis(12, 10, 12);
                var triangle3 = client.WhatShapeIsThis(2, 3, 4);

                //Console.WriteLine(bigFib);
                Console.WriteLine(tokenResult);
                Console.WriteLine(words1);
                Console.WriteLine(words2);
                Console.WriteLine(triangle1);
                Console.WriteLine(triangle2);
                Console.WriteLine(words3);
            }
        }
    }
}
