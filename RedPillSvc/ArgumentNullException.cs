﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace RedPillSvc
{
    [DataContract]
    public class ArgumentNullException
    {
        public string FaultMessage { get; set; }

        public ArgumentNullException(string faultMessage)
        {
            FaultMessage = faultMessage;
        }
    }
}