﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RedPillSvc
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class RedPill : IRedPill
    {
        private const double phi = 1.61803398874989484820458683436563811772030917980576;
        private const string myToken = "06104712-f82e-43f0-9e2a-1d1fddd73a1e";

        /// <summary>
        /// Retrieve the unique Readify application token
        /// </summary>
        /// <returns></returns>
        public Guid WhatIsYourToken()
        {
            return Guid.Parse(myToken);
        }

        /// <summary>
        /// Calculate the Fibonacci number at index fibIndex
        /// </summary>
        /// <param name="fibIndex">The index to calculate the Fibonacci number, can be positive or negative</param>
        /// <returns>The Fibonacci number at index fibIndex</returns>
        public long FibonacciNumber(long n)
        {
            if (n > 92 || n < -92) throw new FaultException("calculateFibonacci(>92 || <92) will cause a 64-bit integer over/underflow.");

            try
            {
                return calculateFibonacci(n);
            }
            catch (Exception e)
            {
                throw new FaultException("Error computing Fibonacci number: " + e.Message);
            }
        }

        private long calculateFibonacci(long fibIndex)
        {
            if (fibIndex == 0) return 0;
            if (fibIndex == 1) return 1;

            // Use Binet's formula
            var fibValue = ((Math.Pow(phi, fibIndex)) - (Math.Pow(-1, fibIndex) / (Math.Pow(phi, fibIndex)))) / Math.Sqrt(5);

            return (long)Math.Floor(fibValue);
        }

        /// <summary>
        /// Determine the type of triangle
        /// </summary>
        /// <param name="a">The length of side A</param>
        /// <param name="b">The length of side B</param>
        /// <param name="c">The length of side C</param>
        /// <returns></returns>
        public TriangleType WhatShapeIsThis(int a, int b, int c)
        {
            return calcTriangle(a, b, c);
        }

        private TriangleType calcTriangle(int a, int b, int c)
        {
            if (a <= 1 || b <= 1 || c <= 1) return TriangleType.Error;

            var triangleSides = new List<int> { a, b, c };

            // Eq
            if (triangleSides.Distinct().Count() == 1) return TriangleType.Equilateral;

            // Iso
            if (triangleSides.Distinct().Count() == 2) return TriangleType.Isosceles;

            // Final check
            if (triangleSides.Distinct().Count() == triangleSides.Count) return TriangleType.Scalene;

            return TriangleType.Error;
        }

        /// <summary>
        /// Reverse the words in a string
        /// </summary>
        /// <param name="s">The source string</param>
        /// <returns>The source string with all letters/words in reverse</returns>
        public string ReverseWords(string s)
        {
            if (string.IsNullOrWhiteSpace(s)) return s;

            try
            {
                var stringChars = s.ToCharArray();

                Array.Reverse(stringChars);

                return new string(stringChars);
            }
            catch (Exception e)
            {
                throw new FaultException("Error reversing words in string: " + e.Message);
            }
        }
    }
}
