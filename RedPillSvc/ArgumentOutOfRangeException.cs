﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace RedPillSvc
{
    [DataContract]
    public class ArgumentOutOfRangeException
    {
        public string FaultMessage { get; set; }

        public ArgumentOutOfRangeException(string faultMessage)
        {
            FaultMessage = faultMessage;
        }
    }
}